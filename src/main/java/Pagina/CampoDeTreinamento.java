package Pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamento {

	@Test
	public void cadastroSimples() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\kevin\\Downloads\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("file:///C:/Users/kevin/eclipse-workspace/Tetset/componentes.html");
		
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		WebElement sexoM = driver.findElement(By.id("elementosForm:sexo:0"));
		WebElement frango = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		WebElement pizza = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		WebElement cadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		
		cadastrar.click();
		
		Alert alertnome = driver.switchTo().alert();
		String textonome = alertnome.getText();
		Assert.assertEquals("Nome eh obrigatorio", textonome);
		alertnome.accept();
		
		driver.switchTo().window("");
		
		nome.clear();
		nome.sendKeys("Kevin");
		
		sobrenome.clear();
		sobrenome.sendKeys("Matheus");
		
		sexoM.click();
		
		frango.click();
		pizza.click();
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		combo.selectByValue("superior");
		
		Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
		
		WebElement element2 = driver.findElement(By.id("elementosForm:esportes"));
		Select combo2 = new Select(element2);
		combo2.selectByValue("natacao");
		
		Assert.assertEquals("Natacao", combo2.getFirstSelectedOption().getText());
		
		sugestoes.clear();
		sugestoes.sendKeys("Cras condimentum non mauris ut posuere. Ut dignissim pulvinar mi, in lacinia turpis suscipit id. In non interdum massa. Donec tristique non massa vel semper. Donec commodo leo eu ultrices viverra. Fusce vitae libero nisi. Mauris congue feugiat dictum. Nunc blandit sed nisl sit amet laoreet. Nullam egestas nisi odio, id vehicula massa imperdiet ac.");

		WebElement clickAlert = driver.findElement(By.xpath("//table[@id='elementosForm:tableUsuarios']//tr[1]/td[3]/input"));
		
		clickAlert.click();
		
		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Francisco", texto);
		alert.accept();
		
				
		cadastrar.click();
		
	}
}
